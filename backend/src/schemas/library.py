from marshmallow import fields
from src.addons import ma
from src.database import Library
from .item import ItemSchema


class LibrarySimpleSchema(ma.ModelSchema):    
    class Meta:
        model = Library
        fields = ('id', 'title', 'owner_id')
        dump_only = ('id',)


class LibrarySchema(ma.ModelSchema):
    items = fields.List(fields.Nested(ItemSchema))
    
    class Meta:
        model = Library
        fields = ('id', 'title', 'owner_id', 'items')
        dump_only = ('id',)

