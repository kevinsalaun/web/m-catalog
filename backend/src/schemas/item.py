from marshmallow import fields, Schema
from src.addons import ma
from src.database import Item


class ItemSchema(ma.ModelSchema):
    class Meta:
        fields = ('imdb_id', 'title', 'year', 'public_rating', 'owner_rating', 'released', 'runtime', 'genre', 'director', 'writer', 'actors', 'production', 'plot', 'itype', 'poster', 'pouch_number')
        dump_only = ('id',)
        model = Item


    def _serialize_single(self, item, **kwargs):
        data = super(ItemSchema, self)._serialize(item, **kwargs)
        return data

    def _serialize(self, item_or_items, many=False, **kwargs):
        if many:
            return [self._serialize_single(item, **kwargs) for item in item_or_items]
        else:
            return self._serialize_single(item_or_items, **kwargs)

    def _deserialize_single(self, data, **kwargs):
        return super(ItemSchema, self)._deserialize(data, **kwargs)

    def _deserialize(self, data, many=False, **kwargs):
        if many:
            return [self._deserialize_single(row, **kwargs) for row in data]
        else:
            return self._deserialize_single(data, **kwargs)

