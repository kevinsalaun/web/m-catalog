from flask import request
from flask_restplus import Namespace, Resource
from flask_jwt_extended import jwt_required

from ..dao import ItemDAO, LibraryDAO

libraries_ns = Namespace('Librarys', '...', path='/')


@libraries_ns.route('/libraries')
class LibrarysResource(Resource):
    def get(self):
        """
        Get libraries
        """
        libraries = LibraryDAO.filter()
        return LibraryDAO.dump(libraries, many=True)

    def post(self):
        """
        Create a library
        """
        library = LibraryDAO.create(request.json)
        return LibraryDAO.dump(library)


@libraries_ns.route('/libraries/<int:library_id>')
class LibraryResource(Resource):
    def get(self, library_id):
        """
        Get a library and its items
        """
        library = LibraryDAO.get_or_404(library_id)
        return LibraryDAO.dump(library, schema='full')


@libraries_ns.route('/libraries/<int:library_id>/items')
class LibraryItemsResource(Resource):
    def post(self, library_id):
        """
        Add items to a library
        """
        items = LibraryDAO.add_items(library_id, request.json)
        return ItemDAO.dump(items, many=True)

    def put(self, library_id):
        """
        Edit items of a library (require to resend all to avoid ordering conflict with async)
        """
        items = LibraryDAO.update_items(library_id, request.json)
        return ItemDAO.dump(items, many=True)


@libraries_ns.route('/items/<string:imdb_id>/external_ids')
class EditExternalIds(Resource):
    @jwt_required
    def put(self, imdb_id):
        """
        Edit external ids.
        """