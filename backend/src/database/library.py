from src.addons import db


class LibraryItem(db.Model):
    __tablename__ = 'library_item'
    id           = db.Column(db.Integer, primary_key=True)
    library_id   = db.Column(db.Integer, db.ForeignKey('library.id'))
    item_imdb_id = db.Column(db.Integer, db.ForeignKey('item.imdb_id'))
    item         = db.relationship('Item')

class Library(db.Model):
    id          = db.Column(db.Integer, primary_key=True)
    owner_id    = db.Column(db.ForeignKey('user.id'))
    title       = db.Column(db.String)

    @property
    def items(self):
        return [
            li.item for li in LibraryItem.query.filter_by(library_id=self.id).all()
        ]

    @items.setter
    def items(self, items):
        LibraryItem.query.filter_by(library_id=self.id).delete()
        db.session.add_all([
            LibraryItem(library_id=self.id, item=item)
            for item in items
        ])
        db.session.commit()

