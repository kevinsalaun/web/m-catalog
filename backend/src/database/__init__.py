from src.addons import db

from src.database.user import User
from src.database.account import Account, Provider
from src.database.friendship import Friendship

from src.database.item import Item
from src.database.library import Library, LibraryItem


def create_db(app):
    db.init_app(app)
    
    with app.app_context():
        db.drop_all()
        db.create_all()
        populate_db_friendship(db)

def populate_db_friendship(db):
    user1 = User(email="user1@gmail.com", password="toto", name="user1")
    user2 = User(email="user2@gmail.com", password="toto", name="user2")
    user3 = User(email="user3@gmail.com", password="toto", name="user3")
    user4 = User(email="user4@gmail.com", password="toto", name="user4")

    db.session.add_all([user1, user2, user3, user4])

    user1_user2 = Friendship(friend1=user1, friend2=user2, accepted=True)
    user1_user3 = Friendship(friend1=user1, friend2=user3, accepted=True)

    db.session.add_all([user1_user2, user1_user3])

    db.session.commit()

