from src.addons import db
import enum


class ItemType(enum.Enum):
    MOVIE   = "MOVIE"
    SERIE   = "SERIE"
    EPISODE = "EPISODE"

class Item(db.Model):
    id            = db.Column(db.Integer, primary_key=True)
    imdb_id       = db.Column(db.String)
    
    title         = db.Column(db.String)
    year          = db.Column(db.Integer)
    public_rating = db.Column(db.String)
    owner_rating  = db.Column(db.String)
    released      = db.Column(db.DateTime)
    runtime       = db.Column(db.String)
    genre         = db.Column(db.String)
    director      = db.Column(db.String)
    writer        = db.Column(db.String)
    actors        = db.Column(db.String)
    production    = db.Column(db.String)
    plot          = db.Column(db.String)
    itype         = db.Column(db.Enum(ItemType))
    poster        = db.Column(db.String)
    pouch_number  = db.Column(db.Integer)
