from ._dao import DAO, db
from .item import ItemDAO
from ..database import Library, LibraryItem
from ..schemas import LibrarySchema, LibrarySimpleSchema

class LibraryDAO(DAO):
    model = Library
    schemas = {
        'default': LibrarySimpleSchema,
        'full': LibrarySchema
    }

    @classmethod
    def add_items(cls, library_id, items_data):
        """
        Add items to this library
        """
        library = cls.get_or_404(library_id)
        items = ItemDAO.create_all(items_data)
        
        db.session.add_all(
            [LibraryItem(item=item, library_id=library_id) for item in items]
        )
        db.session.commit()

        print('after add items', library.items)

        return library.items

    @classmethod
    def drop_items(cls, library_id):
        """
        Drop all items contains in this library
        """
        LibraryItem.query.filter_by(library_id=library_id).delete()
        db.session.commit()


    @classmethod
    def update_items(cls, library_id, items_data):
        """
        Replace item of library with those contained in items data
        """
        cls.drop_items(library_id)
        items = cls.add_items(library_id, items_data)
        print([item.title for item in items])
        return items
