from sqlalchemy import or_
from ._dao import DAO, db
from ..database import Item
from ..schemas import ItemSchema
from ..exceptions import HttpError

class ItemDAO(DAO):
    model = Item
    schemas = {
        'default': ItemSchema
    }

    @classmethod
    def get_or_create(cls, item_data={}, commit=True):
        if not item_data.get('imdb_id'):
            raise HttpError("No IMDB_ID", 400)

        existings = cls.filter(Item.imdb_id == item_data['imdb_id'])

        instance = existings[0] if len(existings) > 0 else None

        if not instance:
            instance = cls.create(item_data, commit=commit)

        return instance


    @classmethod
    def create_all(cls, items_data):
        items = []
        for item_data in (items_data or []):
            items.append( cls.get_or_create(item_data, commit=False) )
        db.session.commit()
        return items
