import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MatListModule } from '@angular/material/list';

import { AuthenticatedGuard } from './guard/authenticated.guard';
import { NotAuthenticatedGuard } from './guard/not-authenticated.guard';

import { LoginComponent } from './view/login/login.component';
import { RegisterComponent } from './view/register/register.component';
import { TileGridComponent } from './view/tile-grid/tile-grid.component';
import { NewItemComponent } from './view/new-item/new-item.component';
import { DetailsComponent } from './view/details/details.component';
import { FriendshipComponent } from './view/friendship/friendship.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent, canActivate: [NotAuthenticatedGuard] },
  { path: 'register', component: RegisterComponent, canActivate: [NotAuthenticatedGuard] },
  { path: 'tiles', component: TileGridComponent, canActivate: [AuthenticatedGuard] },
  { path: 'new-item', component: NewItemComponent, canActivate: [AuthenticatedGuard] },
  { path: 'details', component: DetailsComponent, canActivate: [AuthenticatedGuard] },
  { path: 'friendship', component: FriendshipComponent, canActivate: [AuthenticatedGuard] }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule, MatListModule]
})
export class AppRoutingModule { }

