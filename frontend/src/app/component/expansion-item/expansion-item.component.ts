import { Component, OnInit, Input } from '@angular/core';
import { Item } from 'src/app/types/library';

@Component({
  selector: 'app-expansion-item',
  templateUrl: './expansion-item.component.html',
  styleUrls: ['./expansion-item.component.scss']
})
export class ExpansionItemComponent implements OnInit {
  @Input() tile: Item;

  constructor() { }

  ngOnInit() {
  }

}
