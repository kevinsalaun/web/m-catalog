enum ItemType {
    MOVIE = "MOVIE",
    SERIE = "SERIE",
    EPISODE = "EPISODE"
}

export interface Library {
    id: string,
    owner_id: string,
    title: string,
    items: Item[]
}

export interface Item {
    id: number;
    imdb_id: string;
    title: string;
    year: number;
    public_rating: string;
    owner_rating: string;
    released: string;  // Date
    runtime: string;
    genre: string;
    director: string;
    writer: string;
    actors: string;
    production: string;
    plot: string;
    itype: ItemType;
    poster: string;
}