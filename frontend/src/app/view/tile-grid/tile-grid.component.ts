import { Component, OnInit, Input } from '@angular/core';
import { Library } from 'src/app/types/library';

@Component({
  selector: 'app-tile-grid',
  templateUrl: './tile-grid.component.html',
  styleUrls: ['./tile-grid.component.scss']
})
export class TileGridComponent implements OnInit {
  @Input() tiles: Object[];

  constructor(private library: Library) {
    //TODO: library = libraryService.load(library_id)
  }

  ngOnInit() {
  }

}
