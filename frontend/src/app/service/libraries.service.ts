import { Injectable } from '@angular/core';
import * as uuid from 'uuid';
import * as moment from 'moment';
import { BehaviorSubject } from 'rxjs';
import { StorageService } from './storage.service';
import { Library, Item } from '~types/index';

@Injectable({
  providedIn: 'root'
})
export class LibrarysService {
  private LIBRARIES_BY_USER_KEY = 'libraries';
  // TODO: remove mock
  private currentUser = { id: 1, name: 'Jean Dupont' };

  // TODO: remove this
  private _libraries: { [key: string]: Library } = {};
  private librariesSubject = new BehaviorSubject<Library[]>([]);
  readonly libraries = this.librariesSubject.asObservable();

  constructor(private storage: StorageService) {
    this.loadLibrarys();
  }

  flushData() {
    this.storage.clear();
  }

  notifyLibrarysChange() {
    /**
     * Sauvegarde des libraries dans le localStorage
     */
    // recupere les libraries de tous les utilisateurs
    const allUsersLibrarys: any = this.storage.get(this.LIBRARIES_BY_USER_KEY) || {};
    // ecrase les libraries de l'utilisateur courant par la valeur en mémoire
    allUsersLibrarys[this.currentUser.id] = this._libraries;
    // sauvegarde tout ca dans le localstorage
    this.storage.set(this.LIBRARIES_BY_USER_KEY, allUsersLibrarys);

    /**
     * Notifie les observeurs du changement.
     */
    this.librariesSubject.next(
      Object.values(this._libraries)
    );
  }

  loadLibrarys() {
    // recupere les libraries de tous les utilisateurs
    const allUsersLibrarys = this.storage.get(this.LIBRARIES_BY_USER_KEY) || {};
    // recupere les libraries de l'utilisateur courant
    this._libraries = allUsersLibrarys[this.currentUser.id] || {};
    // averti les observeurs d'un changement des libraries
    this.notifyLibrarysChange();
  }


  create(library: Library) {
    this._libraries[library.id] = library;
    this.notifyLibrarysChange();
  }

  update(libraryId, updateData: { id?, title?, author?}) {
    // modifie un à un les champs de la library concernee
    Object.keys(updateData).map(
      key => {
        this._libraries[libraryId][key] = updateData[key];
      }
    );
    this.notifyLibrarysChange();
  }

  removeLibrary(id: string) {
    delete this._libraries[id];
    this.notifyLibrarysChange();
  }

  libraryGen(title: string = null): string {
    // TODO: Bonne idée, à personnaliser selon des préférences utilisateur
    const date = new Date();
    if (title == null) {
      const dateISO = date.toISOString();
      const formattedDate = moment(dateISO).format('DD/MM/YYYY HH:mm');
      title = 'New Library ' + formattedDate;
    }
    const id = 'library-' + uuid.v4();
    const newLibrary: Library = {
      id,
      cover: '/assets/icons/default.svg',
      title,
      author: this.currentUser.name,
      items: []
    };
    this.create(newLibrary);
    return id;
  }

  /*
  About items
  */

  /**
   * Get nth item of a library
   */
  getItemAt(libraryId: string, index: number): Item {
    const library = this._libraries[libraryId];
    const item = !!library && library.items[index];
    return item;
  }

  /**
   *
   * @param library: The given library.
   * @param item: The item to append.
   */
  addItems(library: Library, ...items: Item[]): Library {
    items.forEach((item: Item) => this._libraries[library.id].items.push(item));
    this.notifyLibrarysChange();
    return this._libraries[library.id];
  }

  /**
   * Add a item to a library
   */
  addItem(libraryId: string, item: Item) {
    this._libraries[libraryId].items.push(item);
    this.notifyLibrarysChange();
  }

  /**
   * Remove a item by index in a library
   */
  delItem(libraryId: string, index: number) {
    this._libraries[libraryId].items.splice(index, 1);
    this.notifyLibrarysChange();
  }

  /**
   * Re-index a given item in a library
   */
  swapItems(libraryId: string, oldIndex: number, newIndex: number) {
    const items = this._libraries[libraryId].items.splice(oldIndex, 1);
    this._libraries[libraryId].items.splice(newIndex, 0, ...items);
    this.notifyLibrarysChange();
  }
}
