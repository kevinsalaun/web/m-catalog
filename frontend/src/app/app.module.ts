import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TileComponent } from './component/tile/tile.component';
import { TileGridComponent } from './view/tile-grid/tile-grid.component';
import { NewItemComponent } from './view/new-item/new-item.component';
import { ExpansionItemComponent } from './component/expansion-item/expansion-item.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DetailsComponent } from './view/details/details.component';

@NgModule({
  declarations: [
    AppComponent,
    TileComponent,
    TileGridComponent,
    NewItemComponent,
    ExpansionItemComponent,
    DetailsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
